package clasesDeObjetosDelSistema;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;



/**
 * Class Calificacion
 */
@Entity
public class Calificacion {

  //
  // Fields
  //
  @Id @GeneratedValue
  private Long id;
  private int puntaje;
  @ManyToOne @JoinColumn(name="Recorrido_id")
  private Recorrido recorrido;
  @ManyToOne @JoinColumn(name="Calificador_id")
  private Usuario calificador;
  @ManyToOne @JoinColumn(name="Calificado_id")
  private Usuario calificado;
  
  //
  // Constructors
  //
  public Calificacion () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of puntaje
   * @param newVar the new value of puntaje
   */
    public void setId(Long id){
	  this.id = id;
	  
  }
  public Long getId(){
	  return this.id;
  }
  public void setPuntaje ( int newVar ) {
    puntaje = newVar;
  }

  /**
   * Get the value of puntaje
   * @return the value of puntaje
   */
  public int getPuntaje ( ) {
    return puntaje;
  }

  /**
   * Set the value of recorrido
   * @param newVar the new value of recorrido
   */
  public void setRecorrido ( Recorrido newVar ) {
    recorrido = newVar;
  }

  /**
   * Get the value of recorrido
   * @return the value of recorrido
   */
  public Recorrido getRecorrido ( ) {
    return recorrido;
  }

  /**
   * Set the value of calificador
   * @param newVar the new value of calificador
   */
  public void setCalificador ( Usuario newVar ) {
    calificador = newVar;
  }

  /**
   * Get the value of calificador
   * @return the value of calificador
   */
  public Usuario getCalificador ( ) {
    return calificador;
  }

  /**
   * Set the value of calificado
   * @param newVar the new value of calificado
   */
  public void setCalificado ( Usuario newVar ) {
    calificado = newVar;
  }

  /**
   * Get the value of calificado
   * @return the value of calificado
   */
  public Usuario getCalificado ( ) {
    return calificado;
  }

  //
  // Other methods
  //

}
