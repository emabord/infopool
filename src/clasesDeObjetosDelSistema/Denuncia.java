package clasesDeObjetosDelSistema;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;



/**
 * Class Denuncia
 */
@Entity
public class Denuncia {

  //
  // Fields
  //
 @Id @GeneratedValue
  private Long id;
 @ManyToOne @JoinColumn(name="Denunciante_id")
  private Usuario denunciante;
 @ManyToOne @JoinColumn(name="Denunciado_id")
  private Usuario denunciado;
 @ManyToOne @JoinColumn(name="Recorrido_id")
  private Recorrido recorrido;
  private String texto;
  
  //
  // Constructors
  //
  public Denuncia () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of denunciante
   * @param newVar the new value of denunciante
   */
    public void setId(Long id){
	  this.id = id;
	  
  }
  public Long getId(){
	  return this.id;
  }
  public void setDenunciante ( Usuario newVar ) {
    denunciante = newVar;
  }

  /**
   * Get the value of denunciante
   * @return the value of denunciante
   */
  public Usuario getDenunciante ( ) {
    return denunciante;
  }

  /**
   * Set the value of denunciado
   * @param newVar the new value of denunciado
   */
  public void setDenunciado ( Usuario newVar ) {
    denunciado = newVar;
  }

  /**
   * Get the value of denunciado
   * @return the value of denunciado
   */
  public Usuario getDenunciado ( ) {
    return denunciado;
  }

  /**
   * Set the value of recorrido
   * @param newVar the new value of recorrido
   */
  public void setRecorrido ( Recorrido newVar ) {
    recorrido = newVar;
  }

  /**
   * Get the value of recorrido
   * @return the value of recorrido
   */
  public Recorrido getRecorrido ( ) {
    return recorrido;
  }

  /**
   * Set the value of texto
   * @param newVar the new value of texto
   */
  public void setTexto ( String newVar ) {
    texto = newVar;
  }

  /**
   * Get the value of texto
   * @return the value of texto
   */
  public String getTexto ( ) {
    return texto;
  }

  //
  // Other methods
  //

}
