package clasesDeObjetosDelSistema;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;



/**
 * Class Recorrido
 */
@Entity
public class Recorrido {

  //
  // Fields
  //
  @Id @GeneratedValue
  private Long id;
  private String tipo_viajero;
  private String salida;
  private String destino;
  @ManyToOne @JoinColumn(name="Evento_id")
  private Evento evento;
  private String recorrido_gmap;
  private Timestamp hora_salida;
  private Timestamp hora_llegada;
  private Date fecha_salida;
  private boolean ida_vuelta;
  @ManyToOne @JoinColumn(name="Creador_id")
  private Usuario creador;
  @ManyToMany
  private List <Usuario> participantes;
  private int asientos;
  private Timestamp hora_regreso;
  @ManyToMany
  private List <Dia> dia;
  public Date fecha_fin;
  
  //
  // Constructors
  //
  @SuppressWarnings("unused")
public Recorrido () {
	List<Usuario> participantes = new ArrayList<Usuario>();
	List<Dia> dia = new ArrayList<Dia>();
	   };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of tipo_viajero
   * @param newVar the new value of tipo_viajero
   */
  
   public void setId(Long id){
	  this.id = id;
	  
  }
  public Long getId(){
	  return this.id;
  }
  
  public void setFechaFin(Date fecha){
	  this.fecha_fin = fecha;
	  
 }
 public Date getFechaFin(){
	  return this.fecha_fin;
 }
  public void setTipo_viajero ( String newVar ) {
    tipo_viajero = newVar;
  }

  /**
   * Get the value of tipo_viajero
   * @return the value of tipo_viajero
   */
  public String getTipo_viajero ( ) {
    return tipo_viajero;
  }

  /**
   * Set the value of salida
   * @param newVar the new value of salida
   */
  public void setSalida ( String newVar ) {
    salida = newVar;
  }

  /**
   * Get the value of salida
   * @return the value of salida
   */
  public String getSalida ( ) {
    return salida;
  }

  /**
   * Set the value of destino
   * @param newVar the new value of destino
   */
  public void setDestino ( String newVar ) {
    destino = newVar;
  }

  /**
   * Get the value of destino
   * @return the value of destino
   */
  public String getDestino ( ) {
    return destino;
  }

  /**
   * Set the value of evento
   * @param newVar the new value of evento
   */
  public void setEvento ( Evento newVar ) {
    evento = newVar;
  }

  /**
   * Get the value of evento
   * @return the value of evento
   */
  public Evento getEvento ( ) {
    return evento;
  }

  /**
   * Set the value of recorrido_gmap
   * @param newVar the new value of recorrido_gmap
   */
  public void setRecorrido_gmap ( String newVar ) {
    recorrido_gmap = newVar;
  }

  /**
   * Get the value of recorrido_gmap
   * @return the value of recorrido_gmap
   */
  public String getRecorrido_gmap ( ) {
    return recorrido_gmap;
  }

  /**
   * Set the value of hora_salida
   * @param newVar the new value of hora_salida
   */
  public void setHora_salida ( Timestamp newVar ) {
    hora_salida = newVar;
  }

  /**
   * Get the value of hora_salida
   * @return the value of hora_salida
   */
  public Timestamp getHora_salida ( ) {
    return hora_salida;
  }

  /**
   * Set the value of hora_llegada
   * @param newVar the new value of hora_llegada
   */
  public void setHora_llegada ( Timestamp newVar ) {
    hora_llegada = newVar;
  }

  /**
   * Get the value of hora_llegada
   * @return the value of hora_llegada
   */
  public Timestamp getHora_llegada ( ) {
    return hora_llegada;
  }

  /**
   * Set the value of fecha_salida
   * @param newVar the new value of fecha_salida
   */
  public void setFecha_salida ( Date newVar ) {
    fecha_salida = newVar;
  }

  /**
   * Get the value of fecha_salida
   * @return the value of fecha_salida
   */
  public Date getFecha_salida ( ) {
    return fecha_salida;
  }

  /**
   * Set the value of ida_vuelta
   * @param newVar the new value of ida_vuelta
   */
  public void setIda_vuelta ( boolean newVar ) {
    ida_vuelta = newVar;
  }

  /**
   * Get the value of ida_vuelta
   * @return the value of ida_vuelta
   */
  public boolean getIda_vuelta ( ) {
    return ida_vuelta;
  }

  /**
   * Set the value of creador
   * @param newVar the new value of creador
   */
  public void setCreador ( Usuario newVar ) {
    creador = newVar;
  }

  /**
   * Get the value of creador
   * @return the value of creador
   */
  public Usuario getCreador ( ) {
    return creador;
  }

  /**
   * Set the value of participantes
   * @param newVar the new value of participantes
   */
  public void setParticipantes ( List <Usuario> newVar ) {
    participantes = newVar;
  }

 public void addParticipante ( Usuario newVar ) {
    participantes.add(newVar);
  }
  /**
   * Get the value of participantes
   * @return the value of participantes
   */
  public List <Usuario> getParticipantes ( ) {
    return (List<Usuario>) participantes;
  }

  /**
   * Set the value of asientos
   * @param newVar the new value of asientos
   */
  public void setAsientos ( int newVar ) {
    asientos = newVar;
  }

  /**
   * Get the value of asientos
   * @return the value of asientos
   */
  public int getAsientos ( ) {
    return asientos;
  }

  /**
   * Set the value of hora_regreso
   * @param newVar the new value of hora_regreso
   */
  public void setHora_regreso ( Timestamp newVar ) {
    hora_regreso = newVar;
  }

  /**
   * Get the value of hora_regreso
   * @return the value of hora_regreso
   */
  public Timestamp getHora_regreso ( ) {
    return hora_regreso;
  }

  /**
   * Set the value of dia
   * @param newVar the new value of dia
   */
  public void setDia ( List <Dia> newVar ) {
    dia = newVar;
  }

	public void addDia ( Dia newVar ) {
    dia.add(newVar);
  }
  /**
   * Get the value of dia
   * @return the value of dia
   */
  public List <Dia> getDia ( ) {
    return (List<Dia>) dia;
  }

  //
  // Other methods
  //

}
