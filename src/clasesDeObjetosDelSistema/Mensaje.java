package clasesDeObjetosDelSistema;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

/**
 * Class Mensaje
 */
@Entity
public class Mensaje {

  //
  // Fields
  //
  @Id @GeneratedValue
  private Long id;
  private String asunto;
  @ManyToMany
  private List<Usuario> destinatarios;
  @ManyToOne @JoinColumn(name="Remitente_id")
  private Usuario remitente;
  private String cuerpo;
  private Date fecha;
  @ManyToMany
  private List<Mensaje> respuesta;
  
  //
  // Constructors
  //
  @SuppressWarnings("unused")
public Mensaje () { 
	
	  List<Usuario> destinatarios = new ArrayList<Usuario>();
	  List<Mensaje> respuesta = new ArrayList<Mensaje>();
	  };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of asunto
   * @param newVar the new value of asunto
   */
   public void setId(Long id){
	  this.id = id;
	  
  }
  public Long getId(){
	  return this.id;
  }
  public void setAsunto ( String newVar ) {
    asunto = newVar;
  }

  /**
   * Get the value of asunto
   * @return the value of asunto
   */
  public String getAsunto ( ) {
    return asunto;
  }

  /**
   * Set the value of destinatario
   * @param newVar the new value of destinatario
   */
  public void setDestinatarios ( List<Usuario> newVar ) {
    destinatarios = newVar;
  }

public void addDestinatario ( Usuario newVar ) {
    destinatarios.add(newVar);
  }
  /**
   * Get the value of destinatario
   * @return the value of destinatario
   */
  public List<Usuario> getDestinatarios ( ) {
    return destinatarios;
  }

  /**
   * Set the value of remitente
   * @param newVar the new value of remitente
   */
  public void setRemitente ( Usuario newVar ) {
    remitente = newVar;
  }

  /**
   * Get the value of remitente
   * @return the value of remitente
   */
  public Usuario getRemitente ( ) {
    return remitente;
  }

  /**
   * Set the value of cuerpo
   * @param newVar the new value of cuerpo
   */
  public void setCuerpo ( String newVar ) {
    cuerpo = newVar;
  }

  /**
   * Get the value of cuerpo
   * @return the value of cuerpo
   */
  public String getCuerpo ( ) {
    return cuerpo;
  }

  /**
   * Set the value of fecha
   * @param newVar the new value of fecha
   */
  public void setFecha ( Date newVar ) {
    fecha = newVar;
  }

  /**
   * Get the value of fecha
   * @return the value of fecha
   */
  public Date getFecha ( ) {
    return fecha;
  }

  /**
   * Set the value of respuesta
   * @param newVar the new value of respuesta
   */
  public void setRespuesta ( List<Mensaje> newVar ) {
    respuesta = newVar;
  }
  
	public void addRespuesta ( Mensaje newVar ) {
    respuesta.add(newVar);
  }
  /**
   * Get the value of respuesta
   * @return the value of respuesta
   */
  public List<Mensaje> getRespuesta ( ) {
    return respuesta;
  }

  //
  // Other methods
  //

}
