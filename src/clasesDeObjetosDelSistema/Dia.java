
package clasesDeObjetosDelSistema;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Class Dia
 */
@Entity
public class Dia {

  //
  // Fields
  //
   @Id @GeneratedValue	
   private Long id;
   private String nombre;
  
  //
  // Constructors
  //
  public Dia () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of nombre
   * @param newVar the new value of nombre
   */
  public void setId(Long id){
	  this.id = id;
	  
  }
  public Long getId(){
	  return this.id;
  }
  public void setNombre ( String newVar ) {
    nombre = newVar;
  }

  /**
   * Get the value of nombre
   * @return the value of nombre
   */
  public String getNombre ( ) {
    return nombre;
  }

  //
  // Other methods
  //

}
