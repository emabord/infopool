package clasesDAO;


import java.util.List;

import clasesDeObjetosDelSistema.Denuncia;


public interface DenunciaDAO {
	
	public Denuncia save(Denuncia d);
	public void delete(Denuncia d);
	public void update(Denuncia d);
	public Denuncia findById(Long id);
	public List<Denuncia>all();
	List<Denuncia> findByRecorrido(Long idr);
	List<Denuncia> findByDenunciado(Long idd);
	List<Denuncia> findByDenunciante(Long idc);

}
