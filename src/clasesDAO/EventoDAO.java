package clasesDAO;



import java.util.Date;
import java.util.List;

import clasesDeObjetosDelSistema.Evento;


public interface EventoDAO {
	public Evento save(Evento e);
	public void delete(Evento e);
	public void update(Evento e);
	public Evento findById(Long id);
	public List<Evento> all();
	public Evento exist(String nombre, Date fecha);

}
