package clasesDAO;

import java.util.List;

import clasesDeObjetosDelSistema.Mensaje;

public interface MensajeDAO {
	public Mensaje save( Mensaje m);
	public void delete(Mensaje m);
	public void update(Mensaje m);
	public Mensaje findById(Long id);
	public List<Mensaje> all();
	public List<Mensaje>findByRemitente(Long idc);

}
