package clasesDAO;

import java.util.List;

import clasesDeObjetosDelSistema.Calificacion;

public interface CalificacionDAO {

		
		public Calificacion save(Calificacion c);
		public void delete(Calificacion c);
		public void update(Calificacion c);
		public Calificacion findById(Long id);
		public List<Calificacion>all();
		List<Calificacion> findByRecorrido(Long idr);
		List<Calificacion> findByCalificado(Long idc);
		List<Calificacion> findByCalificador(Long idc);



}
