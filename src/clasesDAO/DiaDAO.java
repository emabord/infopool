package clasesDAO;

import java.util.List;

import clasesDeObjetosDelSistema.Dia;


public interface DiaDAO {
	public Dia save(Dia d);
	public void delete(Dia d);
	public void update(Dia d);
	public Dia findById(Long id);
	public List<Dia> all();

}
