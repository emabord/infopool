package clasesDAO;

import java.util.List;

import clasesDeObjetosDelSistema.Rol;

public interface RolDAO {
	public Rol save(Rol r);
	public void delete(Rol r);
	public void update(Rol r);
	public Rol findById(Long id);
	public List<Rol>all();
	public Rol exist(Rol r);

}
