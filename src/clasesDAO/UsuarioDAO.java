/**
 * 
 */
package clasesDAO;

import java.util.List;

import clasesDeObjetosDelSistema.Usuario;




/**
 * @author emabord
 *
 */
public interface UsuarioDAO {
	
	public Usuario save(Usuario u ); 
	public void delete(Usuario u);
	public void update(Usuario u);
	public Usuario findById(Long id);
	public List<Usuario>all();
	public  Usuario exist(Usuario u);
	

}
