package clasesDAO;


import java.util.List;

import clasesDeObjetosDelSistema.Recorrido;


public interface RecorridoDAO {
	public Recorrido save(Recorrido r);
	public void delete(Recorrido r);
	public void update(Recorrido r);
	public Recorrido findById(Long id);
	public List<Recorrido> all();
	public List<Recorrido> findByEvento(Long id);
	public List<Recorrido>  findByCreador(Long idc);

}
