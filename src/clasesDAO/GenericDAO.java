package clasesDAO;

import java.util.List;





public interface GenericDAO<T> {
	public  T save(T p);
	public void delete(T p);
	public void update(T p);
	public T exist(T p);
	public T findById(Long id);
	public List<T> all();
	

}
