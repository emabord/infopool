package clasesDAOHiberJPA;

import java.util.List;

import hibernateConf.EMF;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;



import clasesDAO.RecorridoDAO;
import clasesDeObjetosDelSistema.Recorrido;

public class RecorridoHiberJPA extends GenericDAOHiberJPA<Recorrido> implements
		RecorridoDAO {

	

	@Override
	public Recorrido findById(Long id) {
		Recorrido resultado;
		EntityManager em = EMF.getEMF().createEntityManager();
		EntityTransaction etx = em.getTransaction();		
		etx.begin();
		resultado = (Recorrido) em.find(Recorrido.class,id);
		em.flush();
		etx.commit();
		em.close();
		return resultado;
	}
	@Override
	@SuppressWarnings("unchecked")
	public List<Recorrido> findByEvento(Long idc){
		List<Recorrido> resultado = null;
		EntityManager em = EMF.getEMF().createEntityManager();
		Query consulta = em.createQuery("from Recorrido where Evento_id = ?");
		consulta.setParameter(1, idc);
		resultado =  consulta.getResultList();
		return resultado;
	}
	@Override
	@SuppressWarnings("unchecked")
	public List<Recorrido> findByCreador(Long idc){
		List<Recorrido> resultado = null;
		EntityManager em = EMF.getEMF().createEntityManager();
		Query consulta = em.createQuery("from Recorrido where Creador_id = ?");
		consulta.setParameter(1, idc);
		resultado = consulta.getResultList();
		return resultado;
	}
	


}
