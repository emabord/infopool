package clasesDAOHiberJPA;

import hibernateConf.EMF;

import java.util.List;

import javax.management.Query;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;



import clasesDAO.GenericDAO;


/*public class GenericDAOHiberJPA<T> implements GenericDAO <T>{
	protected Class<T> persistentClass;
	private EntityManager entityManager;
	@Override
	public T save(T p) {
		this.getEntityManager().persist(p);
		return p;
	}

	@Override
	public void delete(T r) {
		this.getEntityManager().remove(this.getEntityManager().contains(r) ? r : this.getEntityManager().merge(r));
		

	}

	@Override
	public void update(T p) {
		// TODO Auto-generated method stub
		this.getEntityManager().merge(p);
;
	}

	@Override
	public T findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> all() {
		Query consulta = (Query) this.getEntityManager().createQuery("select * from "+getPersistentClass().getSimpleName());
		List<T> resultados = null;
		resultados = ((javax.persistence.Query) consulta).getResultList();
		return resultados;
	}

	protected Class<T> getPersistentClass() {
		// TODO Auto-generated method stub
		return persistentClass;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	
	public T exist(T objeto) {
		// TODO Auto-generated method stub
		return null;
	}
	
*/
public class GenericDAOHiberJPA<T> implements GenericDAO <T>{
	protected Class<T> persistentClass;
	@Override
	public T save(T p) {
		EntityManager em = EMF.getEMF().createEntityManager();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		em.persist(p);
		em.flush();
		etx.commit();
		em.close();
		return p;
	}

	@Override
	public void delete(T r) {
		// TODO Auto-generated method stub
		EntityManager em = EMF.getEMF().createEntityManager();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		em.remove(em.contains(r) ? r : em.merge(r));
		em.flush();
		etx.commit();
		em.close();

	}

	@Override
	public void update(T p) {
		// TODO Auto-generated method stub
		EntityManager em = EMF.getEMF().createEntityManager();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		em.merge(p);
		em.flush();
		etx.commit();
		em.close();
	}

	@Override
	public T findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> all() {
		List<T> resultados = null;
		Query consulta= (Query) EMF.getEMF().createEntityManager().createQuery("select * from "+getPersistentClass().getSimpleName());
		resultados = ((javax.persistence.Query) consulta).getResultList();
		return resultados;
	}

	protected Class<T> getPersistentClass() {
		// TODO Auto-generated method stub
		return persistentClass;
	}

	@Override
	public T exist(T p) {
		// TODO Auto-generated method stub
		return p;
	}
	

	

	
}

	

