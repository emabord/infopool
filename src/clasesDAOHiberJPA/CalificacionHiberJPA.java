package clasesDAOHiberJPA;

import java.util.List;

import hibernateConf.EMF;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;



import clasesDAO.CalificacionDAO;
import clasesDeObjetosDelSistema.Calificacion;





public class CalificacionHiberJPA extends GenericDAOHiberJPA<Calificacion> implements CalificacionDAO {
	@Override
	public Calificacion findById(Long id) {
		Calificacion resultado;
		EntityManager em = EMF.getEMF().createEntityManager();
		EntityTransaction etx = em.getTransaction();		
		etx.begin();
		resultado = (Calificacion) em.find(Calificacion.class,id);
		em.flush();
		etx.commit();
		em.close();
		return resultado;
	}
	@SuppressWarnings("unchecked")
	@Override
	
	public List<Calificacion> findByRecorrido(Long idr){
		List<Calificacion> resultado = null;
		EntityManager em = EMF.getEMF().createEntityManager();
		Query consulta = em.createQuery("from Calificacion where Recorrido_Id = ?");
		consulta.setParameter(1, idr);
		resultado = consulta.getResultList();
		return resultado;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Calificacion> findByCalificado(Long idc){
		List<Calificacion> resultado = null;
		EntityManager em = EMF.getEMF().createEntityManager();
		Query consulta = em.createQuery("from Calificacion where Calificado_id = ?");
		consulta.setParameter(1, idc);
		resultado = ((javax.persistence.Query) consulta).getResultList();
		return resultado;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Calificacion> findByCalificador(Long idc){
		List<Calificacion> resultado = null;
		EntityManager em = EMF.getEMF().createEntityManager();
		Query consulta = em.createQuery("from Calificacion where Calificador_id = ?");
		consulta.setParameter(1, idc);
		resultado =  consulta.getResultList();
		return resultado;
	}

}
