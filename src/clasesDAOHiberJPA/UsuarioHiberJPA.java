package clasesDAOHiberJPA;
import hibernateConf.EMF;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;



import javax.persistence.NoResultException;
import javax.persistence.Query;

import clasesDAO.UsuarioDAO;
import clasesDeObjetosDelSistema.Usuario;

public class UsuarioHiberJPA extends GenericDAOHiberJPA<Usuario> implements UsuarioDAO {

	

	@Override
	public Usuario findById(Long id) {
		Usuario resultado;
		EntityManager em = EMF.getEMF().createEntityManager();
		EntityTransaction etx = em.getTransaction();		
		etx.begin();
		resultado = (Usuario) em.find(Usuario.class,id);
		em.flush();
		etx.commit();
		em.close();
		return resultado;
	}

	@Override
	public Usuario exist(Usuario u) {
		// TODO Auto-generated method stub
		return null;
	}

	public static Usuario exist(String nombre, String clave) {
		Query resultado;
		Usuario u = null;
		EntityManager em = EMF.getEMF().createEntityManager();
		EntityTransaction etx = em.getTransaction();		
		etx.begin();		
		resultado =  em.createQuery("select u from Usuario u where u.nombre = :nombre and u.clave = :clave"); 
		resultado.setParameter("nombre", nombre);
		resultado.setParameter("clave", clave);	
		try{
			u = (Usuario) resultado.getSingleResult();
		}catch (NoResultException nre){
			
			}
	
		
		em.flush();
		etx.commit();
		em.close();
		return u;
	}

	public static boolean exist(String email) {
		// TODO Auto-generated method stub
		return false;
	}




}
