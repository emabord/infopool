package clasesDAOHiberJPA;



import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hibernateConf.EMF;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;




import javax.persistence.NoResultException;
import javax.persistence.Query;

import clasesDAO.EventoDAO;
import clasesDeObjetosDelSistema.Evento;

public class EventoHiberJPA extends GenericDAOHiberJPA<Evento> implements
		EventoDAO {

	
	@Override
	public Evento findById(Long id) {
		Evento resultado;
		EntityManager em = EMF.getEMF().createEntityManager();
		EntityTransaction etx = em.getTransaction();		
		etx.begin();
		resultado = (Evento) em.find(Evento.class,id);
		em.flush();
		etx.commit();
		em.close();
		return resultado;
	}


	public  Evento exist(String nombre, Date fecha) {
		Query resultado;
		Evento e = null;
		EntityManager em = EMF.getEMF().createEntityManager();
		EntityTransaction etx = em.getTransaction();		
		etx.begin();		
		resultado =  em.createQuery("select e from Evento e where e.nombre = :nombre and e.fecha = :fecha"); 
		resultado.setParameter("nombre", nombre);
		resultado.setParameter("fecha", fecha);	
		try{
			e = (Evento) resultado.getSingleResult();
		}catch (NoResultException nre){
			
			}
	
		
		em.flush();
		etx.commit();
		em.close();
		return e;
	}
	
	public static  Evento existe(String nombre, Date fecha) {
		Query resultado;
		Evento e = null;
		EntityManager em = EMF.getEMF().createEntityManager();
		EntityTransaction etx = em.getTransaction();		
		etx.begin();		
		resultado =  em.createQuery("select e from Evento e where e.nombre = :nombre and e.fecha = :fecha"); 
		resultado.setParameter("nombre", nombre);
		resultado.setParameter("fecha", fecha);	
		try{
			e = (Evento) resultado.getSingleResult();
		}catch (NoResultException nre){
			
			}
	
		
		em.flush();
		etx.commit();
		em.close();
		return e;
	}
	
	public static  List<Evento> todos() {
		Query resultado;
		List<Evento> e = new ArrayList<Evento>();
		EntityManager em = EMF.getEMF().createEntityManager();
		EntityTransaction etx = em.getTransaction();		
		etx.begin();		
		resultado =  em.createQuery("from Evento "); 
		
		try{
			e =  resultado.getResultList();
		}catch (NoResultException nre){
			
			}
	
		
		em.flush();
		etx.commit();
		em.close();
		return e;
	}

}
