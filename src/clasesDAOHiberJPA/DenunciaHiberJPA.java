package clasesDAOHiberJPA;

import java.util.List;

import hibernateConf.EMF;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;



import javax.persistence.Query;


import clasesDAO.DenunciaDAO;
import clasesDeObjetosDelSistema.Denuncia;


public class DenunciaHiberJPA extends GenericDAOHiberJPA<Denuncia>
		implements DenunciaDAO {



	@Override
	public Denuncia findById(Long id) {
		Denuncia resultado;
		EntityManager em = EMF.getEMF().createEntityManager();
		EntityTransaction etx = em.getTransaction();		
		etx.begin();
		resultado = (Denuncia) em.find(Denuncia.class,id);
		em.flush();
		etx.commit();
		em.close();
		return resultado;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Denuncia> findByRecorrido(Long idr){
		List<Denuncia> resultado = null;
		EntityManager em = EMF.getEMF().createEntityManager();
		Query consulta =   em.createQuery("from Denuncia where Recorrido_id = ?");
		consulta.setParameter(1, idr);
		resultado =  consulta.getResultList();
		return resultado;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Denuncia> findByDenunciado(Long idd){
		List<Denuncia> resultado = null;
		EntityManager em = EMF.getEMF().createEntityManager();
		Query consulta = em.createQuery("from Denuncia where Denunciado_id = ?");
		consulta.setParameter(1, idd);
		resultado = consulta.getResultList();
		return resultado;
	}
	@SuppressWarnings("unchecked")
	@Override
	
	public List<Denuncia> findByDenunciante(Long idc){
		List<Denuncia> resultado = null;
		EntityManager em = EMF.getEMF().createEntityManager();
		Query consulta = em.createQuery("from Denuncia where Denunciante_id = ?");
		consulta.setParameter(1, idc);
		resultado =  consulta.getResultList();
		return resultado;
	}


}
