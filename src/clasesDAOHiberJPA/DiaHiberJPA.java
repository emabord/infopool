package clasesDAOHiberJPA;

import hibernateConf.EMF;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;



import clasesDAO.DiaDAO;
import clasesDeObjetosDelSistema.Dia;

public class DiaHiberJPA extends GenericDAOHiberJPA<Dia> implements DiaDAO {

	
	@Override
	public Dia findById(Long id) {
		Dia resultado;
		EntityManager em = EMF.getEMF().createEntityManager();
		EntityTransaction etx = em.getTransaction();		
		etx.begin();
		resultado = (Dia) em.find(Dia.class,id);
		em.flush();
		etx.commit();
		em.close();
		return resultado;
	}

}
