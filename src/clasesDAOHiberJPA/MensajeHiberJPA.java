package clasesDAOHiberJPA;



import java.util.List;

import hibernateConf.EMF;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;



import clasesDAO.MensajeDAO;
import clasesDeObjetosDelSistema.Mensaje;



public class MensajeHiberJPA extends GenericDAOHiberJPA<Mensaje> implements
		MensajeDAO {

		@Override
	public Mensaje findById(Long id) {
			Mensaje resultado;
			EntityManager em = EMF.getEMF().createEntityManager();
			EntityTransaction etx = em.getTransaction();		
			etx.begin();
			resultado = (Mensaje) em.find(Mensaje.class,id);
			em.flush();
			etx.commit();
			em.close();
			return resultado;
	}
		@SuppressWarnings("unchecked")
		@Override
		public List<Mensaje> findByRemitente(Long idc){
			List<Mensaje> resultado = null;
			EntityManager em = EMF.getEMF().createEntityManager();
			Query consulta =  (Query) em.createQuery("from Mensaje where Remitente_id = ?");
			consulta.setParameter(1, idc);
			resultado = consulta.getResultList();
			return resultado;
		}


}
