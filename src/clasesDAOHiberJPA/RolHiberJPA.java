package clasesDAOHiberJPA;







import hibernateConf.EMF;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;



import javax.persistence.NoResultException;
import javax.persistence.Query;

import clasesDAO.RolDAO;
import clasesDeObjetosDelSistema.Rol;


public class RolHiberJPA extends GenericDAOHiberJPA<Rol> implements RolDAO  {

	@Override
	public Rol findById(Long id) {
		Rol resultado;
		EntityManager em = EMF.getEMF().createEntityManager();
		EntityTransaction etx = em.getTransaction();		
		etx.begin();
		resultado = (Rol) em.find(Rol.class,id);
		em.flush();
		etx.commit();
		em.close();
		return resultado;
	}
	
	@Override
	public Rol exist(Rol r){
		Rol u = null;
		Query resultado;
		EntityManager em = EMF.getEMF().createEntityManager();
		EntityTransaction etx = em.getTransaction();		
		etx.begin();		
		resultado =  em.createQuery("select r from Rol r where r.nombre = :nombre"); 
		resultado.setParameter("nombre", r.getNombre());
		try{
			 u = (Rol) resultado.getSingleResult();
		}catch (NoResultException nre){
			
		}
		em.flush();
		etx.commit();
		em.close();
		return u;
	}

	public static Rol exist(String nombre) {
		Rol u = null;
		Query resultado;
		EntityManager em = EMF.getEMF().createEntityManager();
		EntityTransaction etx = em.getTransaction();		
		etx.begin();		
		resultado =  em.createQuery("select r from Rol r where r.nombre = :nombre"); 
		resultado.setParameter("nombre", nombre);
		try{
			 u = (Rol) resultado.getSingleResult();
		}catch (NoResultException nre){
			
		}
		em.flush();
		etx.commit();
		em.close();
		return u;
	}

	

	
}
