package converters;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;

import org.apache.struts2.util.StrutsTypeConverter;

import com.opensymphony.xwork2.conversion.TypeConversionException;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;



public class StringToDateTimeConverter extends StrutsTypeConverter {

	private static final DateFormat DATETIME_FORMAT = new SimpleDateFormat("yyyy/MM/dd hh:mm");

    @SuppressWarnings("rawtypes")
	public Object convertFromString(Map context, String[] strings, Class toClass) {     
        if (strings == null || strings.length == 0 || strings[0].trim().length() == 0) {
            return null;
        }

        try {
            return DATETIME_FORMAT.parse(strings[0]);
        } catch (ParseException e) {
            throw new TypeConversionException("Unable to convert given object to date: " + strings[0]);
        } catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return toClass;
    }

    @SuppressWarnings("rawtypes")
	public String convertToString(Map context, Object date) {
        if (date != null && date instanceof Date) {         
            return DATETIME_FORMAT.format(date);
        } else {
            return null;
        }
    }
}