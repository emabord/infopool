package FactoryDAO;

import clasesDAO.CalificacionDAO;
import clasesDAO.DenunciaDAO;
import clasesDAO.DiaDAO;
import clasesDAO.EventoDAO;
import clasesDAO.MensajeDAO;
import clasesDAO.RecorridoDAO;
import clasesDAO.RolDAO;
import clasesDAO.UsuarioDAO;
import clasesDAOHiberJPA.CalificacionHiberJPA;
import clasesDAOHiberJPA.DenunciaHiberJPA;
import clasesDAOHiberJPA.DiaHiberJPA;
import clasesDAOHiberJPA.EventoHiberJPA;
import clasesDAOHiberJPA.MensajeHiberJPA;
import clasesDAOHiberJPA.RecorridoHiberJPA;
import clasesDAOHiberJPA.RolHiberJPA;
import clasesDAOHiberJPA.UsuarioHiberJPA;


public class FactoryDAOHiberJPA {
	
	public static UsuarioDAO getUsuarioDAO(){
		return new UsuarioHiberJPA();
	}
	public static CalificacionDAO getCalificacionDAO(){
		return new CalificacionHiberJPA();
	}
	public static RolDAO getRolDAO() {
		// TODO Auto-generated method stub
		return new RolHiberJPA();
	}
	public static DiaDAO getDiaDAO() {
		// TODO Auto-generated method stub
		return new DiaHiberJPA();
	}
	public static EventoDAO getEventoDAO() {
		// TODO Auto-generated method stub
		return new EventoHiberJPA();
	}
	public static DenunciaDAO getDenunciaDAO() {
		// TODO Auto-generated method stub
		return new DenunciaHiberJPA();
	}
	public static RecorridoDAO getRecorridoDAO() {
		// TODO Auto-generated method stub
		return new RecorridoHiberJPA();
	}
	public static MensajeDAO getMensajeDAO() {
		// TODO Auto-generated method stub
		return new MensajeHiberJPA();
	}
	

}
