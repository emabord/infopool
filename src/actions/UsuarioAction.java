package struts2;



import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.Validations;



@SuppressWarnings("serial")
@Namespace(value="/")
@Action(value="/usuario", results={@Result(name="input", location="/login.jsp"),
								   @Result(name="success", location="/usuario.jsp")
								   })

@Validations
public class UsuarioAction extends ActionSupport  {
	/**
	 * 
	 */
	
	String nombre;
	String clave;
	String perfil;

	public String execute() {
		return SUCCESS;
		}
	
	/*public void validate(){
		
		if (getNombre()==null)
			addFieldError("nombre", "debe ingresar un nombre de usuario");
		if(getClave()==null)
		addFieldError("clave", "el campo de la clave no puede estar vacío");
		

	

	}*/
	@RequiredStringValidator(message="El nombre de usuario no puede quedar en blanco") 
	public String getNombre() {
		// TODO Auto-generated method stub
		return nombre;
	}
	public void setNombre(String identificador){
		this.nombre = identificador;
	}
	
	public void setClave(String clave){
		this.clave = clave;
	}
	@RequiredStringValidator(message="Debe ingresar una clave")
	public String getClave() {
		// TODO Auto-generated method stub
		return clave;
	}
	
	public String getPerfil(){
		return perfil;
	}
	public void setPerfil(String perfil){
		this.perfil = perfil;
	}
}
