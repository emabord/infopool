package actions;

import java.io.File;







import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Blob;
import java.util.Map;










import org.hibernate.Hibernate;
import org.hibernate.Session;

import FactoryDAO.FactoryDAOHiberJPA;
import clasesDAO.RolDAO;
import clasesDAO.UsuarioDAO;
import clasesDAOHiberJPA.RolHiberJPA;
import clasesDAOHiberJPA.UsuarioHiberJPA;
import clasesDeObjetosDelSistema.Rol;
import clasesDeObjetosDelSistema.Usuario;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class Register extends ActionSupport{

    
	
	public String nombre;
	public String apellido;
	public String telefono;
	public String email;
	public String clave;
	public String clave2;
	public File foto;
	private UsuarioDAO usuarioDAO;




	public String execute() throws IOException {
		
		Map<String, Object> session = ActionContext.getContext().getSession();
		Usuario user = (Usuario) session.get("usuario");		
		if (user == null){			
			if ((getClave() != null) && getClave().equals(getClave2()) ){
					
				Usuario u = UsuarioHiberJPA.exist(getNombre(), getClave());
				if (u == null){
					
					u = new Usuario();
					u.setApellido(getApellido());
					u.setClave(getClave());
					u.setEmail(getEmail());
					if (getFoto()!=null){
						byte[] bFile = new byte[(int)getFoto().length()];
						FileInputStream inputStream = new FileInputStream(getFoto());
						inputStream.read(bFile);
						inputStream.close();
						u.setImagen(bFile);
						//debería redimensionar la imagen que se vea bien
					}
			        u.setNombre(getNombre());
					u.setTelefono(getTelefono());
					Rol r =	RolHiberJPA.exist("viajero");
					if (r == null){
						r = new Rol();
						r.setNombre("viajero");
						RolDAO rolDAO = FactoryDAOHiberJPA.getRolDAO();
						rolDAO.save(r);
					}
					u.setRol(r);
					UsuarioDAO usuarioDAO = FactoryDAOHiberJPA.getUsuarioDAO();
					usuarioDAO.save(u);
					return SUCCESS;
				}else{
					if (getClave() == null)
						return "register";
					else
						return INPUT;
				}
				
			}else{
				if (getClave() == null)
					return "register";
				else
					return INPUT;
			}
		}else{
			
			return user.getRol().getNombre();
		}
		
		
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getClave() {
		return clave;
	}


	public void setClave(String clave) {
		this.clave = clave;
	}


	public File getFoto() {
		return foto;
	}


	public void setFoto(File foto) {
		this.foto = foto;
	}


	public String getClave2() {
		return clave2;
	}


	public void setClave2(String clave2) {
		this.clave2 = clave2;
	}


	public UsuarioDAO getUsuarioDAO() {
		return usuarioDAO;
	}


	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}







}
