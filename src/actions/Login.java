package actions;

import java.util.Map;











import clasesDAO.UsuarioDAO;
import clasesDAOHiberJPA.UsuarioHiberJPA;
import clasesDeObjetosDelSistema.Usuario;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;


@SuppressWarnings("serial")
public class Login extends ActionSupport  {
	
	
	String nombre;
	String clave;


	private UsuarioDAO usuarioDAO = null;

	@Override
	public String execute(){
		
		
		Map<String, Object> session = ActionContext.getContext().getSession();
		Usuario user = (Usuario) session.get("usuario");	
		System.out.println(user);
		if (user == null){
			
			Usuario u = UsuarioHiberJPA.exist(this.getNombre(), this.getClave());
			if (u != null){
				
				session.put("perfil", u.getRol().getNombre());
				session.put("nombre", u.getNombre());
				session.put("usuario", u);				
				return u.getRol().getNombre();

			}else{
				return "input";

				}
		}else{
			return user.getRol().getNombre();

			}
			
		}
		

	@RequiredStringValidator(message="El nombre de usuario no puede quedar en blanco") 
	public String getNombre() {
		// TODO Auto-generated method stub
		return nombre;
	}
	public void setNombre(String identificador){
		this.nombre = identificador;
	}
	
	public void setClave(String clave){
		this.clave = clave;
	}
	@RequiredStringValidator(message="Debe ingresar una clave")
	public String getClave() {
		// TODO Auto-generated method stub
		return clave;
	}
	
	
	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
		}
	public UsuarioDAO getUsuarioDAO() {
		return this.usuarioDAO ;
		}

}
