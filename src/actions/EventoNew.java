package actions;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import FactoryDAO.FactoryDAOHiberJPA;
import clasesDAO.EventoDAO;
import clasesDAOHiberJPA.EventoHiberJPA;
import clasesDeObjetosDelSistema.Evento;
import clasesDeObjetosDelSistema.Usuario;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.conversion.annotations.Conversion;
import com.opensymphony.xwork2.conversion.annotations.TypeConversion;

@Conversion
public class EventoNew extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private EventoDAO evento;
	
	String nombre;
	Date fecha;
	String sitio_web;
	String lugar;
	private List<Evento> eventos = new ArrayList<Evento>();
	
	public String execute(){
		Map<String, Object> session = ActionContext.getContext().getSession();
		Usuario user = (Usuario) session.get("usuario");		
		if (user == null){
			return "login";
		}else{
			if (!(user.getRol().getNombre().equals("admin"))){
				return "error";
			}else{
				if (getNombre() != null){

					Evento e = EventoHiberJPA.existe(getNombre(),getFecha());

					if (e == null){
						e = new Evento();
						e.setFecha(getFecha());
						e.setLugar(getLugar());
						e.setNombre(getNombre());
						e.setSitio_web(getSitioWeb());
						evento = FactoryDAOHiberJPA.getEventoDAO();
						getEvento().save(e);
						return "add_evento_ok";
					}else
						return "add_evento_existe";
					
				}else{
					return "add_evento";
					
				}
				
			}
		}
		
	}
	public String list(){	
		Map<String, Object> session = ActionContext.getContext().getSession();
		Usuario user = (Usuario) session.get("usuario");		
		if (user == null){
			return "login";
		}else{
			if (!(user.getRol().getNombre().equals("admin"))){
				return "error";
			}else{
				setEventos(EventoHiberJPA.todos()); 
				System.out.println(eventos);
				return "success";
			}
		}
		
	}
	public void setLugar(String lugar){
		this.lugar = lugar;
	}
	public String getLugar(){
		return lugar;
	}
	
	public void setSitioWeb(String sitio_web){
		this.sitio_web = sitio_web;
	}
	public String getSitioWeb(){
		return sitio_web;
	}

	
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	
	private String getNombre() {
		// TODO Auto-generated method stub
		return nombre;
	}

	@TypeConversion(converter="converters.StringToDateTimeConverter")
	public void setFecha(Date fecha){
		this.fecha = fecha;
	}

	private Date getFecha() {
		// TODO Auto-generated method stub
		return fecha;
	}




	public EventoDAO getEvento() {
		return evento;
	}
	public void setEvento(EventoDAO evento) {
		this.evento = evento;
	}
	
	public List<Evento> getEventos(){
		return eventos;
	}
	public void setEventos(List<Evento> eventos){
		this.eventos = eventos;
	}
}
